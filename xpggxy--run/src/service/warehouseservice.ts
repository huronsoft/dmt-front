import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { WareHouse } from "../domain/warehouse";
import { Rack } from "@domain/rack";

@Injectable({
    providedIn: 'root'
})
export class WareHouseService {
    private warhousefindall = "http://localhost:8080/findall";
    private saveURL = 'http://localhost:8080/save';
    private saveRackURL = 'http://localhost:8080/rack';
    private delUrl = 'http://localhost:8080/delete';
    private findRacksUrl = 'http://localhost:8080/findbywarehouseid/';
    private deleteRackUrl = 'http://localhost:8080/deleterack';

    constructor(private http: HttpClient) { }

    addWareHouse(warehouse: WareHouse): Observable<any> {
        const headers = { 'content-type': 'application/json', 'Access-Control-Allow-Origin': 'http://localhost:4200' }
        const body = JSON.stringify(warehouse);
        return this.http.post(this.saveURL, body, { 'headers': headers })
    }

    updateWareHouse(warehouse: WareHouse): Observable<any> {
        const headers = { 'content-type': 'application/json', 'Access-Control-Allow-Origin': 'http://localhost:4200' }
        const body = JSON.stringify(warehouse);
        return this.http.put(this.saveURL, body, { 'headers': headers })
    }

    addRack(rack: Rack): Observable<any> {
        const headers = { 'content-type': 'application/json', 'Access-Control-Allow-Origin': 'http://localhost:4200' }
        const body = JSON.stringify(rack);
        console.log(body)
        return this.http.post(this.saveRackURL, body, { 'headers': headers })
    }

    deleteWareHouse(warehouse: WareHouse) {
        const headers = { 'content-type': 'application/json', 'Access-Control-Allow-Origin': 'http://localhost:4200' }
        const body = JSON.stringify(warehouse);
        return this.http.post(this.delUrl, body, { 'headers': headers })
    }

    deleteRack(rack: Rack) {
        const headers = { 'content-type': 'application/json', 'Access-Control-Allow-Origin': 'http://localhost:4200' }
        const body = JSON.stringify(rack);
        return this.http.post(this.deleteRackUrl, body, { 'headers': headers })
    }

    public findAll(): Observable<WareHouse> {
        return this.http.get<WareHouse>(this.warhousefindall);
    }

    public findbywarehouseid(warehouseId: number): Observable<Rack> {
        return this.http.get<Rack>(this.findRacksUrl + warehouseId);
    }

    getRacksMaster() {
        return [
            { id: 1, type: 'A' }, { id: 2, type: 'B' }, { id: 3, type: 'C' }, { id: 4, type: 'D' }
        ];
    }

    getWareHouseData() {
        return [
            {
                id: 1,
                uuid: '12345678-1234-5678-1234-567812345678',
                clientName: 'element s.a.s',
                family: 'EST',
                size: 3,
                rack: [
                    {
                        id: 1,
                        warehouse_id: 1,
                        uuid: '12345678-5678-1234-1234-567812345678',
                        type: 'A'
                    },
                    {
                        id: 2,
                        warehouse_id: 1,
                        uuid: '43345678-1234-5678-1234-567812345678',
                        type: 'B'
                    },
                    {
                        id: 3,
                        warehouse_id: 1,
                        uuid: '43345678-1234-5678-1234-567812345985',
                        type: 'C'
                    }
                ]
            }
        ]
    }

    getWareHouses() {
        return Promise.resolve(this.getWareHouseData());
    }

    getRackMasters() {
        return Promise.resolve(this.getRacksMaster());
    }

    getWareHousesOther() {
        return Promise.resolve(this.getWareHouseData());
    }
}