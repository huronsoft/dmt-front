import { Rack } from "./rack";

export interface WareHouse {
    id?: number;
    uuid?: string;
    clientName?: string;
    family?: string;
    size?: number;
    rack?: Rack[];
}