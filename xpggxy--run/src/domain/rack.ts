export interface Rack {
    id?: number;
    warehouseId?: number;
    uuid?: string;
    typeRack?: string;
}