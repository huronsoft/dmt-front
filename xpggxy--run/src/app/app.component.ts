import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { ImportsModule } from './imports';
import { ConfirmationService, MessageService } from 'primeng/api';
import { WareHouseService } from '../service/warehouseservice';
import { WareHouse } from '../domain/warehouse';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { Family } from '../domain/family';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { Rack } from '@domain/rack';
import { RackMaster } from '@domain/rackmaster';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, ImportsModule, DialogModule,
    ButtonModule, InputTextModule, FormsModule,
    DropdownModule, ConfirmDialogModule],
  providers: [MessageService, WareHouseService, ConfirmationService],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit {
  title = 'app-dmt';
  warehouses!: WareHouse[];
  warehss: any = [];
  visible: boolean = false;
  visibleEditar: boolean = false;
  display: string = 'no-visible';
  value = 'Almacen de prueba';
  accion = 'Crear almacén';
  accionEditar = 'Editar almacén';
  families: Family[] | any;
  rackMasters: RackMaster[] | any;
  racks: Rack[] | any;
  resp: string;
  selectedFam: Family = {
    id: 0,
    name: null
  }
  selectedFamEdit: Family = {
    id: 0,
    name: null
  }
  selectedRack: RackMaster = {
    id: 0,
    type: ''
  }
  rack: Rack = {
    id: 0,
    warehouseId: 0,
    uuid: '',
    typeRack: ''
  }
  wareHouse: WareHouse = {
    id: 0,
    uuid: '',
    clientName: '',
    family: '',
    size: 0
  }

  constructor(private wareHouseService: WareHouseService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private http: HttpClient) {

  }

  ngOnInit() {
    this.families = [{ id: 1, name: 'EST' }, { id: 2, name: 'ROB' }];
    this.wareHouseService.getRackMasters().then((response) => {
      this.rackMasters = response;
    });
    this.warehss = [];
    this.getWareHouses();
  }

  editarWareHouse(event, warehouse: WareHouse) {
    this.wareHouse = {
      id: warehouse.id,
      uuid: warehouse.uuid,
      clientName: warehouse.clientName,
      family: warehouse.family,
      size: warehouse.size
    }
    this.selectedFamEdit = {
      id: 0,
      name: warehouse.family
    }
    this.getRacksByWareHousewId(warehouse.id);
    this.visibleEditar = true;
  }

  onChangeEdit(event, wareHouse: WareHouse) {
    if (wareHouse.family === 'EST') {
      if (!this.findLetterRacks('D')) {
        wareHouse.family = this.selectedFamEdit.name;
      }
    }
    if (wareHouse.family === 'ROB') {
      if (!this.findLetterRacks('B')) {
        wareHouse.family = this.selectedFamEdit.name;
      }
    }

  }

  findLetterRacks(letter: string) {
    for (var i of this.racks) {
      if (i.typeRack === letter) {
        this.messageService.add({ severity: 'error', summary: 'Cambio de tipo', detail: 'No se puede cambiar de tipo, primero elimine las instalaciones incompatibles' });
        return true;
      }
    }
    return false;
  }


  onChangeRack(event, rack: Rack) {
    rack.typeRack = this.selectedRack.type;
  }

  deleteRack(rack: Rack) {
    this.sendDeleteRack(rack);
  }

  getWareHouses() {
    this.wareHouseService.findAll().subscribe(response => (this.warehss = response));
    //console.log(this.warehss);
  }

  getRacksByWareHousewId(warehouseId: number) {
    this.racks = [];
    this.wareHouseService.findbywarehouseid(warehouseId).subscribe(response => (this.racks = response));
  }

  showDialog() {
    this.reinitWareHouse();
    this.visible = true;
  }

  reinitWareHouse() {
    this.wareHouse = {
      id: 0,
      uuid: '',
      clientName: '',
      family: '',
      size: 0
    }
  }

  createWareHouse() {
    if (this.validWarehouse('cre') === true) {
      this.wareHouse.family = this.selectedFam.name;
      this.sendPostCreate();
    }
  }

  updateWareHouse() {
    this.wareHouse.family = this.selectedFamEdit.name;
    const headers = { 'content-type': 'application/json', 'Access-Control-Allow-Origin': 'http://localhost:4200' }
    if (this.validWarehouse('mod') === true) {
      const body = JSON.stringify(this.wareHouse);
      this.http.put<any>('http://localhost:8080/update', body, { 'headers': headers })
        .subscribe({
          next: data => {
            this.messageService.add({ severity: 'success', summary: 'Almacén modificado', detail: 'Almacén ha sido modificado correctamente' });
            this.getWareHouses();
            this.visibleEditar = false;
          },
          error: error => {
            this.messageService.add({ severity: 'error', summary: 'Error', detail: error.message });
          }
        });
    }
  }

  validWarehouse(accion: string) {
    if (this.selectedFam.name === null && accion === 'cre') {
      this.messageService.add({ severity: 'error', summary: 'Nuevo almacén', detail: 'Debe seleccionar un tipo!' });
      return false;
    }
    if (this.wareHouse.clientName === '') {
      this.messageService.add({ severity: 'error', summary: 'Nuevo almacén', detail: 'Debe ingresar un nombre para el cliente!' });
      return false;
    }
    if (this.wareHouse.size <= 0) {
      this.messageService.add({ severity: 'error', summary: 'Nuevo almacén', detail: 'Debe ingresar un tamaño mayor o igual a uno!' });
      return false;
    }
    return true;
  }

  sendPostCreate() {
    this.wareHouseService.addWareHouse(this.wareHouse)
      .subscribe(data => {
        //console.log(data);
        this.visible = false;
        this.getWareHouses();
        this.messageService.add({ severity: 'success', summary: 'Almacén Creado', detail: 'Nuevo almacén ha sido ingresado correctamente' });
      })
  }

  sendDelete(wareHouse: WareHouse) {
    this.wareHouseService.deleteWareHouse(wareHouse)
      .subscribe(data => {
        //console.log(data);
        this.visible = false;
        this.getWareHouses();
        this.messageService.add({ severity: 'success', summary: 'Eliminar almacén', detail: 'Almacén seleccionado ha sido eliminado correctamente' });
      })
  }

  sendDeleteRack(rack: Rack) {
    this.wareHouseService.deleteRack(rack)
      .subscribe(data => {
        //console.log(data);
        this.getRacksByWareHousewId(this.wareHouse.id);
      })
  }

  addRack() {
    //console.log(this.wareHouse);
    if (this.selectedRack.id <= 0) {
      this.messageService.add({ severity: 'error', summary: 'Rechazado', detail: 'Seleccione un rack válido' });
    } else {
      this.rack = {
        warehouseId: this.wareHouse.id,
        uuid: '',
        typeRack: this.selectedRack.type
      }
      if (this.validRack(this.rack, this.wareHouse.family)) {
        this.wareHouseService.addRack(this.rack).subscribe(data => {
          //console.log(data);
          this.getRacksByWareHousewId(this.wareHouse.id);
        })
      }
    }
  }

  validRack(rack: Rack, family: string) {
    if (family === 'EST') {
      if (rack.typeRack === 'D') {
        this.messageService.add({ severity: 'error', summary: 'Rechazado', detail: 'Rack inválido!' });
        return false;
      }
    }
    if (family === 'ROB') {
      if (rack.typeRack === 'B') {
        this.messageService.add({ severity: 'error', summary: 'Rechazado', detail: 'Rack inválido!' });
        return false;
      }
    }
    return true;
  }

  confirmarEliminar(event: Event, warehouse: WareHouse) {
    //console.log(warehouse.clientName);
    this.confirmationService.confirm({
      target: event.target as EventTarget,
      message: 'Seguro que desea borrar el registro?',
      header: 'Confirmación',
      icon: 'pi pi-info-circle',
      acceptButtonStyleClass: "p-button-danger p-button-text",
      rejectButtonStyleClass: "p-button-text p-button-text",
      acceptIcon: "none",
      rejectIcon: "none",

      accept: () => {
        this.sendDelete(warehouse);
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Rechazado', detail: 'Registro no eliminado' });
      }
    });
  }
}
